package section1_intro.part1_language_basics;

public class Rectangle {
    Point upperLeft;
    Point lowerRight;

    /**
     * This method returns the surface defined by the rectangle with the given Point objects at the upper left and
     * lower right corners.
     * The method assumes two corners have been set already (and are not null).
     * @return surface
     */
    int getSurface(){
        //calculate surface - can you implement this?
        return (upperLeft.y - lowerRight.y) * ( lowerRight.x - upperLeft.x );
    }

    public static void main(String[] args) {
        Point p1 = new Point();
        p1.x = 2;
        p1.y = 8;

        Point p2 = new Point();
        p2.x = 10;
        p2.y = 3;

        Rectangle rectangle = new Rectangle();
        rectangle.upperLeft = p1;
        rectangle.lowerRight = p2;
        System.out.println(rectangle.getSurface());
        System.out.println(5*8);

    }
}
