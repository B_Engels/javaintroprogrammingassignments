package section1_intro.part1_language_basics;


public class Point {
    int x;
    int y;

    /**
     * Create an instance of class point that is located at the same coordinates as the current object, but in the
     * diagonally opposing quadrant of coordinate space.
     * So, when the current point is at (4, 4), this method will return Point(-4, -4)
     * and when the current point is at (2, -5) it will return Point(-2, 5).
     * @return inverse Point
     */
    Point createInversePoint() {
        Point p = new Point();
        p.x = -this.x;
        p.y = -this.y;
        return p;
    }

    /**
     * This method returns the Euclidean distance of the current point (this) to the given point (otherPoint).
     * GIYF if you forgot what Euclidean distance is and how it is calculated.
     * @param otherPoint
     * @return euclidean distance
     */
    double euclideanDistanceTo(Point otherPoint) {
        //YOUR CODE HERE

        double firsPoint = Math.abs(otherPoint.y - y);
        double secondPoint = Math.abs(otherPoint.x - x);

        return Math.hypot(firsPoint,secondPoint);
    }

    public static void main(String[] args) {
        Point point = new Point();
        point.x = 2;
        point.y = -3;
        if (point.createInversePoint().y == 3) System.out.println("true");
        else System.out.println("false");

        Point point1 = new Point();
        point1.x = 2;
        point1.y = 2;

        Point point2 = new Point();
        point2.x = 4;
        point2.y = 4;

        System.out.println(point2.euclideanDistanceTo(point1));



    }

}
