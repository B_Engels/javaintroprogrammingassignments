package section1_intro.part1_language_basics;

public class GeometryAnalyser {
    public static void main(String[] args) {
        //YOUR CODE HERE
        int result = 0;
        if (args[4].equals("dist")){
            Point point1 = new Point();
            point1.x = Integer.parseInt(args[0]);
            point1.y = Integer.parseInt(args[1]);

            Point point2 = new Point();
            point2.x = Integer.parseInt(args[2]);
            point2.y = Integer.parseInt(args[3]);

            result = (int) point2.euclideanDistanceTo(point1);
        }
        if (args[4].equals("surf")){
            Point point1 = new Point();
            point1.x = Integer.parseInt(args[0]);
            point1.y = Integer.parseInt(args[1]);

            Point point2 = new Point();
            point2.x = Integer.parseInt(args[2]);
            point2.y = Integer.parseInt(args[3]);

            Rectangle rectangle = new Rectangle();
            rectangle.upperLeft = point1;
            rectangle.lowerRight = point2;
            result = rectangle.getSurface();
        }
        System.out.println(result);
    }
}
