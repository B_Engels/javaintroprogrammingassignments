package section2_syntax.part5_zoo;

import java.util.List;

public class ZooApp {

    public static void main(String[] args) {
        ZooApp zooApp = new ZooApp();
        zooApp.processZooData(args);
        zooApp.printZooSummary();
    }

    /**
     * Processes the command line data.
     * @param args
     */
    void processZooData(String[] args) {
        //YOUR CODE HERE; pass zoo animals to ZooSpecies
        for (int i = 0; i < args.length; i++) {
            ZooSpecies zooSpecies = new ZooSpecies(args[i]);
            ZooSpecies.registerSpeciesFromString(zooSpecies.getSpeciesName());
        }
    }

    /**
     * Prints a summary of the zoo.
     */
    void printZooSummary() {
        final List<ZooSpecies> allSpecies = ZooSpecies.getAllSpecies(); //YOUR CODE HERE; fetch all species
        //YOUR CODE HERE
        System.out.println("The zoo has " + allSpecies.size() + " species." + "\n" + "These are the species counts:" );

        for (ZooSpecies species : allSpecies) {
            //YOUR CODE HERE
            System.out.println(species.getSpeciesName() + " " + species.getIndividualCount()  );
        }
    }
}
